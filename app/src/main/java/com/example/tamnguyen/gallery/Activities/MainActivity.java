package com.example.tamnguyen.gallery.Activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;

import com.example.tamnguyen.gallery.Fragment.albumsGridFragment;
import com.example.tamnguyen.gallery.Fragment.albumsListFragment;
import com.example.tamnguyen.gallery.Fragment.imageGridFragment;
import com.example.tamnguyen.gallery.Fragment.imageListFragment;
import com.example.tamnguyen.gallery.Fragment.showImageFragment;
import com.example.tamnguyen.gallery.Models.MyAlbum;
import com.example.tamnguyen.gallery.Models.MyCamera;
import com.example.tamnguyen.gallery.Models.MyImage;
import com.example.tamnguyen.gallery.Models.onAlbumImageClickListener;
import com.example.tamnguyen.gallery.Models.onGalleryClickListener;
import com.example.tamnguyen.gallery.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements onGalleryClickListener, onAlbumImageClickListener, SearchView.OnQueryTextListener {
    public static final String CLICKED_IMAGE = "clickedImage";
    public static final String CLICKED_ALBUM = "clickedAlbum";
    public static final int REQCODE_CAMERA = 1;
    public static final int REQCODE_CAMERA_RESULT = 1;
    public static final int SWITCH_TO_GRID = 0;
    public static final int SWITCH_TO_LIST = 1;

    public static final String SEND_BUNDLE = "sendBundle";

    private MyAlbum curAlbum;
    private int menuToInflate = R.menu.main_app_bar_list_menu;

    FragmentManager mFragmentManager;
    String curAlbumName = "";
    public static MyCamera myCam;
    @BindView(R.id.main_toolbar)
    Toolbar myToolbar;

    private SearchView searchView;

    public static MyImage bmInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        myCam = new MyCamera(MainActivity.this);

        setSupportActionBar(myToolbar);
        setup();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void setup() {
        mFragmentManager = getSupportFragmentManager();
        switchFragment(SWITCH_TO_LIST);
    }

    public void switchFragment(int caseId) {
        Fragment frag = null;
        Fragment curFrag = mFragmentManager.findFragmentById(R.id.frameLayout);
        String fragId = "";
        switch (caseId) {
            case SWITCH_TO_GRID: {

                if (curFrag instanceof imageListFragment) {

                    // todo: change label or sth
                    myToolbar.setTitle(curAlbumName);

                    //change fragment
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(CLICKED_ALBUM, curAlbum);
                    frag = new imageGridFragment();
                    frag.setArguments(bundle);


                } else {
                    fragId = "album";
                    frag = new albumsGridFragment();
                }

            }
            break;
            case SWITCH_TO_LIST: {

                if (curFrag instanceof imageGridFragment) {

                    // todo: change label or sth
                    myToolbar.setTitle(curAlbumName);

                    //change fragment
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(CLICKED_ALBUM, curAlbum);
                    frag = new imageListFragment();
                    frag.setArguments(bundle);
                } else {
                    fragId = "album";
                    frag = new albumsListFragment();
                }
            }
            break;
        }

        if (frag != null) {

            if (curFrag instanceof imageGridFragment || curFrag instanceof imageListFragment)
                mFragmentManager.popBackStack();

            mFragmentManager.beginTransaction()
                    .replace(R.id.frameLayout, frag)
                    .addToBackStack(fragId)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment curFrag = mFragmentManager.findFragmentById(R.id.frameLayout);
        String popbackto = "album";
        if (curFrag instanceof imageListFragment) {
            // todo: change label or sth
            myToolbar.setTitle(R.string.txt_main_album);

        } else if (curFrag instanceof showImageFragment) {
            // todo: change label or sth


        } else if (curFrag instanceof imageGridFragment) {
            // todo: change label or sth
            myToolbar.setTitle(R.string.txt_main_album);
        } else
            System.exit(0);

        //redraw menu
        mFragmentManager.popBackStackImmediate(popbackto, 0);
        Fragment albumFragment = mFragmentManager.findFragmentById(R.id.frameLayout);
        if (albumFragment != null) {
            if (albumFragment instanceof albumsGridFragment)
                menuToInflate = R.menu.main_app_bar_grid_menu;
            if (albumFragment instanceof albumsListFragment)
                menuToInflate = R.menu.main_app_bar_list_menu;
        }
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(menuToInflate, menu);
        MenuItem itemSearch = menu.findItem(R.id.menu_action_search);
        searchView = (SearchView) itemSearch.getActionView();
        searchView.setOnQueryTextListener(this);

        if (curAlbumName == "")
            myToolbar.setTitle(R.string.txt_main_album);
        myToolbar.setTitleTextColor(Color.WHITE);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_change_to_grid_view) {

            menuToInflate = R.menu.main_app_bar_grid_menu;
            invalidateOptionsMenu();
            switchFragment(SWITCH_TO_GRID);
        } else if (id == R.id.action_change_to_list_view) {

            menuToInflate = R.menu.main_app_bar_list_menu;
            invalidateOptionsMenu();
            switchFragment(SWITCH_TO_LIST);
        } else if (id == R.id.action_camera) {
            myCam.startCameraIntent();
        } else if (id == R.id.menu_sort_by_name || id == R.id.menu_sort_by_datetaken || id == R.id.menu_sort_by_size) {
            Fragment curFrag = mFragmentManager.findFragmentById(R.id.frameLayout);
            if (curFrag instanceof albumsListFragment)
                ((albumsListFragment) curFrag).sortBy(id);
            if (curFrag instanceof albumsGridFragment)
                ((albumsGridFragment) curFrag).sortBy(id);
            if (curFrag instanceof imageListFragment)
                ((imageListFragment) curFrag).sortBy(id);
            if (curFrag instanceof imageGridFragment)
                ((imageGridFragment) curFrag).sortBy(id);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAlbumClickListener(MyAlbum clickedItem) {
        Fragment curAlbumFrag = mFragmentManager.findFragmentById(R.id.frameLayout);

        // Temporarily initialize, may not be optimized
        Fragment imagesFrag = new Fragment();
        curAlbum = clickedItem;

        if (curAlbumFrag instanceof albumsListFragment) {

            // todo: change label or sth
            curAlbumName = clickedItem.getName();
            myToolbar.setTitle(curAlbumName);

            //change fragment
            Bundle bundle = new Bundle();
            bundle.putSerializable(CLICKED_ALBUM, clickedItem);

            imagesFrag = new imageListFragment();
            imagesFrag.setArguments(bundle);
        } else if (curAlbumFrag instanceof albumsGridFragment) {

            // todo: change label or sth
            curAlbumName = clickedItem.getName();
            myToolbar.setTitle(curAlbumName);

            // Change fragment
            Bundle bundle = new Bundle();
            bundle.putSerializable(CLICKED_ALBUM, clickedItem);

            imagesFrag = new imageGridFragment();
            imagesFrag.setArguments(bundle);
        } else
            System.exit(0);

        mFragmentManager.beginTransaction().replace(R.id.frameLayout, imagesFrag).addToBackStack("").commit();
    }

    @Override
    public void onImageClick(MyImage clickedItem) {
        Fragment curFrag = mFragmentManager.findFragmentById(R.id.frameLayout);

        // Temporarily initialize, may not be optimized
        if (curFrag instanceof imageListFragment)
            ShowImageActivity.startFrom = 0;
        if (curFrag instanceof imageGridFragment)
            ShowImageActivity.startFrom = 1;

        if (curFrag instanceof imageListFragment || curFrag instanceof imageGridFragment) {

            // todo: change label or sth

            //change activity
            Bundle bundle = new Bundle();
            bundle.putSerializable(CLICKED_IMAGE, clickedItem);

            //TODO: call ShowImageActivity
            Intent showImageIntent = new Intent(this, ShowImageActivity.class);
            showImageIntent.putExtra(SEND_BUNDLE, bundle);
            startActivity(showImageIntent);
        } else
            System.exit(0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQCODE_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                myCam.startCameraIntent();
            } else System.exit(0);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQCODE_CAMERA_RESULT) {
            if (resultCode == RESULT_OK) {
                //get image bitmap, add to content resolver
                myCam.broadcastLastTakenImage();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("RESUME", "mainAc onRestart");
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        ArrayList<MyImage> searchResult = searchImages(s);
        MyAlbum tempAlbum = new MyAlbum("", "", searchResult);
        //chuyen fragment
        Fragment curAlbumFrag = mFragmentManager.findFragmentById(R.id.frameLayout);

        // Temporarily initialize, may not be optimized
        Fragment imagesFrag = new Fragment();
        myToolbar.setTitle("Search result");

        if (curAlbumFrag instanceof albumsListFragment || curAlbumFrag instanceof imageListFragment) {
            //change fragment
            Bundle bundle = new Bundle();
            bundle.putSerializable(CLICKED_ALBUM, tempAlbum);

            imagesFrag = new imageListFragment();
            imagesFrag.setArguments(bundle);
        } else if (curAlbumFrag instanceof albumsGridFragment || curAlbumFrag instanceof imageGridFragment) {
            // Change fragment
            Bundle bundle = new Bundle();
            bundle.putSerializable(CLICKED_ALBUM, tempAlbum);
            imagesFrag = new imageGridFragment();
            imagesFrag.setArguments(bundle);
        }
        mFragmentManager.beginTransaction().replace(R.id.frameLayout, imagesFrag).addToBackStack("").commit();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    private ArrayList<MyImage> searchImages(String query) {
        ArrayList<MyImage> result = new ArrayList<>();

        String selection;
        if (query.contains("#")) //tag
            selection = MediaStore.Images.ImageColumns.DESCRIPTION + " LIKE '%" + query.substring(1) + "%'";
        else //name
            selection = MediaStore.Images.ImageColumns.DISPLAY_NAME + " LIKE '%" + query + "%'";
        String defaultOrderASC =
                MediaStore.Images.Media.DISPLAY_NAME
                        + " ASC";

        ContentResolver cr = getContentResolver();
        Cursor cs = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, selection, null, defaultOrderASC);

        while (cs.moveToNext()) {
            MyImage image = new MyImage(cs);
            result.add(image);
        }

        cs.close();
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("RESUME", "mainAC resume");
    }
}
