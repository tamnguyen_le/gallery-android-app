package com.example.tamnguyen.gallery.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.tamnguyen.gallery.Fragment.imageGridFragment;
import com.example.tamnguyen.gallery.Fragment.imageListFragment;
import com.example.tamnguyen.gallery.Models.DrawTextTransformation;
import com.example.tamnguyen.gallery.Models.GlideApp;
import com.example.tamnguyen.gallery.Models.ImageSaver;
import com.example.tamnguyen.gallery.Models.MyCamera;
import com.example.tamnguyen.gallery.Models.MyImage;
import com.example.tamnguyen.gallery.Models.MyImageDecoder;
import com.example.tamnguyen.gallery.Models.RotateTransformation;
import com.example.tamnguyen.gallery.R;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.pes.androidmaterialcolorpickerdialog.ColorPickerCallback;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.tamnguyen.gallery.Fragment.imageListFragment.co;

public class ShowImageActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final float MAXIMUM_SCALE = 5.00f;
    private static final int LOCATION_REQCODE = 1;

    public static int startFrom = -1;

    private int menuToInflate = R.menu.showimage_activity_menu;
    private String fileNameToSave = "";
    private Bitmap curImageState = null;
    private int imageDisplayZoneWidth = 0;
    private int imageDisplayZoneHeight = 0;
    private String titleToSet = "";
    private int rotationAngle = 0;
    private boolean hasCroppedImage = false;

    @BindView(R.id.pvShowImage)
    PhotoView pvImage;

    //@BindView(R.id.ivShowImage)
    //ImageView ivImage;

    @BindView(R.id.showImage_toolbar)
    Toolbar myToolbar;
    @BindView(R.id.cropImageView)
    CropImageView cropImageView;

    private AlertDialog editDialog;
    private MyImage clickedImage;

    private String fileExtension;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Activity curAc;

    //drawtext variables
    private AlertDialog drawtextDialog;
    private String drawString;
    private Integer drawColor;
    private int drawSize = 250;

    private AlertDialog checkDeleteImageDialog;
    private AlertDialog checkSaveImageDialog;
    private android.app.AlertDialog saveFileDialog;
    private ImageButton ibtnAutoFillLoc;

    @Override
    public void onBackPressed() {
        if (menuToInflate == R.menu.showimage_activity_edit_menu) {
            menuToInflate = R.menu.showimage_activity_menu;
            titleToSet = clickedImage.getDisplay_name();
            invalidateOptionsMenu();
        }
        else if (menuToInflate == R.menu.showimage_activity_crop_menu) {

            menuToInflate = R.menu.showimage_activity_edit_menu;
            cropImageView.setVisibility(View.INVISIBLE);
            pvImage.setVisibility(View.VISIBLE);
            invalidateOptionsMenu();
        }
        else {
            if (hasEdittedImage())
                checkSaveImageDialog.show();
            else
                super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        ButterKnife.bind(this);
        setupUI();
        //setSupportActionBar(myToolbar);
        setSupportActionBar(myToolbar);
        setupGoogleApiClient();

        titleToSet = clickedImage.getDisplay_name();
        myToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        myToolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        cropImageView.setOnCropImageCompleteListener(new CropImageView.OnCropImageCompleteListener() {
            @Override
            public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {

                handleCropImageView();
            }
        });
    }

    private void rotate(int rotationAngle) {
        GlideApp
                .with(getApplicationContext())
                .asBitmap()
                .load(curImageState)
                .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                .fitCenter()
                .transform(new RotateTransformation(this, rotationAngle))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        curImageState = resource;
                        pvImage.setImageBitmap(resource);
                    }
                });
    }

    private void setCropImageView(int x, int y) {

        pvImage.setVisibility(View.INVISIBLE);
        cropImageView.setVisibility(View.VISIBLE);
        cropImageView.setAspectRatio(x, y);
        cropImageView.setFixedAspectRatio(true);
        cropImageView.setAutoZoomEnabled(false);
        //cropImageView.setImageUriAsync(Uri.fromFile(new File(clickedImage.getData())));
        cropImageView.setImageBitmap(curImageState);
    }

    private void drawText() {
        if (drawString != null && drawColor != null) {
            GlideApp
                    .with(getApplicationContext())
                    .asBitmap()
                    .load(curImageState)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .fitCenter()
                    .transform(new DrawTextTransformation(this, drawString, drawColor, drawSize))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            curImageState = resource;
                            pvImage.setImageBitmap(resource);
                        }
                    });
        }
    }

    private void handleCropImageView() {

        curImageState = cropImageView.getCroppedImage();
        cropImageView.setVisibility(View.INVISIBLE);
        pvImage.setVisibility(View.VISIBLE);

        GlideApp
                .with(getApplicationContext())
                .asBitmap()
                .load(curImageState)
                .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                .fitCenter()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        curImageState = resource;
                        pvImage.setImageBitmap(resource);
                    }
                });

        menuToInflate = R.menu.showimage_activity_edit_menu;
        invalidateOptionsMenu();
    }

    private MyImage getBundleImage() {
        //TODO: get bundle
        Intent intent = getIntent();
        Bundle send_bundle = intent.getBundleExtra(MainActivity.SEND_BUNDLE);

        //TODO: get MyImage from bundle
        return (MyImage) send_bundle.get(MainActivity.CLICKED_IMAGE);
    }

    public void setupUI() {
        curAc = this;
        //TODO: set image to imageview
        clickedImage = getBundleImage();

        GlideApp
                .with(getApplicationContext())
                .load(clickedImage.getData())
                .fitCenter()
                .into(pvImage);

        //TODO: initialize photoView
        pvImage.setMaximumScale(MAXIMUM_SCALE);

        //TODO: setup edit dialog, progress set wallpaper dialog
        setupEditDialog();
        setupDrawTextDialog();
        setupTextInputForSavingDialog();
        setupCheckDeleteDialog();
        setupCheckSaveDialog();
        //TODO: get file extension
        fileExtension = clickedImage.getDisplay_name().substring(clickedImage.getDisplay_name().lastIndexOf("."));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(menuToInflate, menu);
        myToolbar.setTitle(titleToSet);
        //myToolbar.setVisibility(View.INVISIBLE);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_edit_image) {

            menuToInflate = R.menu.showimage_activity_edit_menu;
            titleToSet = "";
            invalidateOptionsMenu();
            imageDisplayZoneWidth = pvImage.getWidth();
            imageDisplayZoneHeight = pvImage.getHeight();
            if (curImageState == null)
                curImageState = MyImageDecoder.decodeBitmapFromFile(clickedImage.getData(), imageDisplayZoneWidth, imageDisplayZoneHeight);

        }
        else if (id == R.id.action_edit_info) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQCODE);
            }
            else {
                editDialog.show();
            }

        }
        else if (id == R.id.aspect_1x1) {

            cropImageView.setAspectRatio(1, 1);

        }
        else if (id == R.id.aspect_2x3) {

            cropImageView.setAspectRatio(2, 3);

        }
        else if (id == R.id.aspect_16x9) {

            cropImageView.setAspectRatio(16, 9);

        }
        else if (id == R.id.action_rotate_right) {

            if (menuToInflate == R.menu.showimage_activity_edit_menu)
                rotate(90);
            else if (menuToInflate == R.menu.showimage_activity_crop_menu)
                cropImageView.rotateImage(90);

            rotationAngle += 90;
        }
        else if (id == R.id.action_rotate_left) {

            if (menuToInflate == R.menu.showimage_activity_edit_menu)
                rotate(-90);
            else if (menuToInflate == R.menu.showimage_activity_crop_menu)
                cropImageView.rotateImage(-90);

            rotationAngle -= 90;
        }
        else if (id == R.id.action_crop) {

            //Todo: call crop function
            menuToInflate = R.menu.showimage_activity_crop_menu;
            titleToSet = "";
            invalidateOptionsMenu();
            setCropImageView(2, 3);

        }
        else if (id == R.id.action_confirm_crop) {

            cropImageView.getCroppedImageAsync();
            hasCroppedImage = true;
        }
        else if (id == R.id.action_set_wallpaper) {

            new actionSetWallpaper(ShowImageActivity.this).execute();

        }
        else if (id == R.id.action_drawtext) {
            drawtextDialog.show();

        }
        else if (id == R.id.action_save) {

            //todo: save bitmap as new image
            saveFileDialog.show();
        }
        else if (id == R.id.action_delete) {

            checkDeleteImageDialog.show();
        }
        else if (id == R.id.share_to_fb){

            if (curImageState == null){

                imageDisplayZoneWidth = pvImage.getWidth();
                imageDisplayZoneHeight = pvImage.getHeight();
                curImageState = MyImageDecoder.decodeBitmapFromFile(clickedImage.getData(), imageDisplayZoneWidth, imageDisplayZoneHeight);
            }

            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(curImageState)
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();

            ShareDialog.show(this, content);
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mLastLocation == null)
            getLastLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
        if (startFrom == 0)
            getContentResolver().unregisterContentObserver(imageListFragment.co);
        if (startFrom == 1)
            getContentResolver().unregisterContentObserver(imageGridFragment.co);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
        if (startFrom == 0)
            getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, imageListFragment.co);
        if (startFrom == 1)
            getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, imageGridFragment.co);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQCODE) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ibtnAutoFillLoc.setEnabled(false);
            }
            else {
                ibtnAutoFillLoc.setEnabled(true);
                getLastLocation();
            }
            editDialog.show();
        }
    }

    private void setupCheckSaveDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning").setMessage("Save this image?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                saveFileDialog.show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                // set these attributes to allow backPress
                rotationAngle = 0;
                hasCroppedImage = false;
                onBackPressed();
            }
        });

        checkSaveImageDialog = builder.create();
    }

    private void setupCheckDeleteDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning").setMessage("Delete this image?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteImage();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        checkDeleteImageDialog = builder.create();
    }

    private void deleteImage() {

        //todo: fix delete
        String imagePath = clickedImage.getData();
        File file = new File(imagePath);
        file.delete();
        this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(imagePath))));
        //getContentResolver().notifyChange(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null);
        onBackPressed();
    }

    private void setupDrawTextDialog() {
        final Integer[] colorChosen = new Integer[1];

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Option");

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.draw_text_dialog, null);
        builder.setView(dialogView);

        //mapping view
        final EditText drawtext_text = dialogView.findViewById(R.id.drawtext_text);
        final Button drawtext_color = dialogView.findViewById(R.id.drawtext_color);
        SeekBar drawtext_size = dialogView.findViewById(R.id.drawtext_size);
        final TextView drawtext_size_display = dialogView.findViewById(R.id.drawtext_size_display);

        drawtext_size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                drawtext_size_display.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        colorChosen[0] = Color.parseColor("#FF4081");

        drawtext_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //open color picker
                //default color #3F51B5 = RGB(63,81,181)
                final ColorPicker cp = new ColorPicker(ShowImageActivity.this, 255, 64, 129);
                cp.show();

                //callback when select color
                cp.setCallback(new ColorPickerCallback() {
                    @Override
                    public void onColorChosen(int color) {
                        colorChosen[0] = color;
                        drawtext_color.setBackgroundColor(color);
                        cp.cancel();
                    }
                });
            }
        });

        builder.setPositiveButton("Draw", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //draw text
                if (!drawtext_text.getText().equals("")) {
                    drawString = drawtext_text.getText().toString();
                    drawColor = colorChosen[0];
                    drawSize = Integer.parseInt(drawtext_size_display.getText().toString());
                    drawText();
                }
                drawtextDialog.dismiss();
            }

        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //cancel
                drawtextDialog.dismiss();
            }
        });

        //create dialog
        drawtextDialog = builder.create();
    }

    private void setupEditDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Image's information");

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.edit_information_dialog_layout, null);
        builder.setView(dialogView);

        //mapping view
        final EditText edtName = dialogView.findViewById(R.id.edt_info_dlg_imageName);
        final EditText edtDescription = dialogView.findViewById(R.id.edt_info_dlg_description);

//        final TextView path = dialogView.findViewById(R.id.edt_info_dlg_path);

//        final EditText edtLongtitude = dialogView.findViewById(R.id.edt_info_dlg_longtitude);
//        final EditText edtLadtiude = dialogView.findViewById(R.id.edt_info_dlg_ladtitude);
//        edtAddress = dialogView.findViewById(R.id.txt_info_dlg_address);
//        ibtnAutoFillLoc = dialogView.findViewById(R.id.btn_info_dlg_fillLocation);

        //set information
        edtName.setText(clickedImage.getDisplay_name());
        edtDescription.setText(clickedImage.getDescription());

//        path.setText(clickedImage.getData());

//        edtLongtitude.setText(clickedImage.getLongtitude());
//        edtLadtiude.setText(clickedImage.getLadtitude());
        //set address
//        edtAddress.setText(getAddressFromLocation(clickedImage.getLongtitude(), clickedImage.getLadtitude()));

        /*ibtnAutoFillLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLastLocation();
                if (mLastLocation != null) {
                    edtLongtitude.setText(String.valueOf(mLastLocation.getLongitude()));
                    edtLadtiude.setText(String.valueOf(mLastLocation.getLatitude()));
                    edtAddress.setText(getAddressFromLocation(edtLongtitude.getText().toString(), edtLadtiude.getText().toString()));
                }
            }
        });*/

        //build dialog
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //TODO: save edited information (content provider),update current Image information (loaded image)
                //save data
                saveEditedInformation(edtDescription, edtName);
                myToolbar.setTitle(edtName.getText());
                editDialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //TODO: exit dialog
                editDialog.dismiss();
            }
        });

        editDialog = builder.create();
    }

    public void setupTextInputForSavingDialog() {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Save File");

        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.text_input_dialog, null);
        builder.setView(dialogView);

        final EditText input = dialogView.findViewById(R.id.textInput);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fileNameToSave = input.getText().toString();
                ImageSaver.storeImage(curImageState, fileNameToSave, getApplicationContext());
                // set these attributes to allow backPress
                rotationAngle = 0;
                hasCroppedImage = false;
                onBackPressed();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveFileDialog.cancel();
            }
        });

        saveFileDialog = builder.create();
    }

    public void saveEditedInformation(EditText edtDescription, EditText edtImageName) {
        String newDescription = edtDescription.getText().toString();
        String newName = edtImageName.getText().toString();
        //check file extension
        if (!newName.contains(fileExtension)) newName += fileExtension;

        //change current image data
        clickedImage.setDescription(newDescription);
        clickedImage.setDisplay_name(newName);

        //save to provider
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DESCRIPTION, newDescription);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, newName);

        String where = MediaStore.Images.ImageColumns._ID + " LIKE ?";
        getContentResolver().update(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values, where, new String[]{clickedImage.getId()});
        getContentResolver().notifyChange(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null);

        //broadcastLastEditedImage(clickedImage.getData());
    }

    //get location longtitude,ladtitude
    public void setupGoogleApiClient() {
        if (mGoogleApiClient == null)

            mGoogleApiClient = new GoogleApiClient.Builder(this,
                    this, this)
                    .addApi(LocationServices.API).build();
    }


    public void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        curImageState = null;
        super.onDestroy();
    }

    public class actionSetWallpaper extends AsyncTask<String, Void, Void> {
        private Context callerContext;
        ProgressDialog dialog;

        public actionSetWallpaper(Context callerContext) {
            this.callerContext = callerContext;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(callerContext, ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.setMessage("Setting wallpaper ...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            WallpaperManager wm = WallpaperManager.getInstance(getApplicationContext());
            Bitmap bm = BitmapFactory.decodeFile(clickedImage.getData());
            //TODO: scale bitmap to device screen size
            // Bitmap scaleVer = fitImage2Screen(bm);

            try {
                wm.setBitmap(bm);
            } catch (IOException e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            Toast.makeText(callerContext, "Finish", Toast.LENGTH_SHORT).show();
        }
    }


    public boolean hasEdittedImage() {

        if (hasCroppedImage || ((rotationAngle % 360) != 0))
            return true;

        return false;
    }

}
