package com.example.tamnguyen.gallery.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tamnguyen.gallery.Models.GlideApp;
import com.example.tamnguyen.gallery.R;
import com.example.tamnguyen.gallery.Models.MyAlbum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BI on 05-11-17.
 */

public class AlbumListAdapter extends ArrayAdapter<MyAlbum> implements Serializable {
    private Context mContext;
    private int mResource;
    private ArrayList<MyAlbum> myAlbumArrayList;

    public AlbumListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        myAlbumArrayList = (ArrayList<MyAlbum>) objects;
    }

    class viewHolder {
        ImageView ivIcon;
        TextView tvName;
        TextView tvFolderSize;
        TextView tvNumOfPhotos;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        viewHolder mViewHolder;
        MyAlbum album = myAlbumArrayList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);

            mViewHolder = new viewHolder();
            mViewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.ivThumbnail);
            mViewHolder.tvName = (TextView) convertView.findViewById(R.id.tvAlbumName);
            mViewHolder.tvFolderSize = (TextView) convertView.findViewById(R.id.tvAlbumSize);
            mViewHolder.tvNumOfPhotos = (TextView) convertView.findViewById(R.id.tvNumbOfPics);

            convertView.setTag(mViewHolder);
        } else mViewHolder = (viewHolder) convertView.getTag();


        GlideApp
                .with(getContext())
                .load(album.getThumbnailPath())
                .centerCrop()
                .into(mViewHolder.ivIcon);

        mViewHolder.tvName.setText(album.getName());
        mViewHolder.tvFolderSize.setText(album.getAlbumSize());
        mViewHolder.tvNumOfPhotos.setText(album.getNumOfPhotos());

        return convertView;
    }
}
