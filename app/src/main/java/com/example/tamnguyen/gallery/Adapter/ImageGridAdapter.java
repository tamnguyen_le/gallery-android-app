package com.example.tamnguyen.gallery.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.tamnguyen.gallery.Models.GlideApp;
import com.example.tamnguyen.gallery.Models.MyAlbum;
import com.example.tamnguyen.gallery.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.example.tamnguyen.gallery.Models.MyImage;

/**
 * Created by Tam Nguyen on 11-Nov-17.
 */

public class ImageGridAdapter extends ArrayAdapter<MyAlbum> implements Serializable {
    private Context mContext;
    private int mResource;
    private ArrayList<MyImage> myImageArrayList;

    public ImageGridAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        myImageArrayList = (ArrayList<MyImage>) objects;
    }

    class viewHolder {
        ImageView ivIcon;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        viewHolder mViewHolder;
        MyImage image = myImageArrayList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);

            mViewHolder = new viewHolder();
            mViewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.ivThumbnail);
            convertView.setTag(mViewHolder);
        } else mViewHolder = (viewHolder) convertView.getTag();

        GlideApp
                .with(getContext())
                .load(image.getData())
                .centerCrop()
                .into(mViewHolder.ivIcon);

        return convertView;
    }
}