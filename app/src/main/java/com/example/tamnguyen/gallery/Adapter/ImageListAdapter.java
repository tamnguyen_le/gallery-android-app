package com.example.tamnguyen.gallery.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tamnguyen.gallery.Models.GlideApp;
import com.example.tamnguyen.gallery.Models.MyImage;
import com.example.tamnguyen.gallery.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BI on 06-11-17.
 */

public class ImageListAdapter extends ArrayAdapter<MyImage> {
    private Context mContext;
    private int mResource;
    private ArrayList<MyImage> myImageArrayList;

    public ImageListAdapter(@NonNull Context context, int resource, @NonNull List<MyImage> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        myImageArrayList = (ArrayList<MyImage>) objects;
    }


    class viewHolder {
        ImageView ivIcon;
        TextView tvName;
        TextView tvFileSize;
        TextView tvLastEditDate;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ImageListAdapter.viewHolder mViewHolder;
        MyImage image = myImageArrayList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);

            mViewHolder = new ImageListAdapter.viewHolder();
            mViewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.ivThumbnail);
            mViewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            mViewHolder.tvFileSize = (TextView) convertView.findViewById(R.id.tvFileSize);
            mViewHolder.tvLastEditDate = (TextView) convertView.findViewById(R.id.tvLastEditDate);

            convertView.setTag(mViewHolder);
        } else mViewHolder = (ImageListAdapter.viewHolder) convertView.getTag();

        GlideApp
                .with(getContext())
                .load(image.getData())
                .centerCrop()
                .into(mViewHolder.ivIcon);

        mViewHolder.tvName.setText(image.getDisplay_name());
        mViewHolder.tvFileSize.setText(image.getSize());
        mViewHolder.tvLastEditDate.setText(image.getDate_added());

        return convertView;
    }
}
