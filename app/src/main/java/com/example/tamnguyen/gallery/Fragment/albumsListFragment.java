package com.example.tamnguyen.gallery.Fragment;


import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.tamnguyen.gallery.R;
import com.example.tamnguyen.gallery.Adapter.AlbumListAdapter;
import com.example.tamnguyen.gallery.Models.MyAlbum;
import com.example.tamnguyen.gallery.Models.MyGallery;
import com.example.tamnguyen.gallery.Models.onGalleryClickListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class albumsListFragment extends Fragment {

    onGalleryClickListener mOnGalleryClickListener;

    private final static int REQCODE_READ_EXTERNAL_STORAGE = 1;
    @BindView(R.id.lvAlbum)
    ListView lvAlbums;

    AlbumListAdapter adapterAlbum;
    MyGallery gallery;
    ArrayList<MyAlbum> myAlbums;

    Unbinder unbinder;

    private ContentResolver cr;
    ContentObserver co;

    public albumsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.list_layout_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);

        setupUI();
        return v;
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    public void setupGallery() {

        gallery = new MyGallery(getContext());
        myAlbums = new ArrayList<>(gallery.getMyAlbums());

        adapterAlbum = new AlbumListAdapter(getContext(), R.layout.album_item_for_list_view, myAlbums);
        lvAlbums.setAdapter(adapterAlbum);

        lvAlbums.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyAlbum itemClicked = myAlbums.get(position);
                mOnGalleryClickListener.onAlbumClickListener(itemClicked);
            }
        });
    }

    public void setupUI() {
        //check permission
        int perCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (perCheck == PackageManager.PERMISSION_GRANTED) {
            setupGallery();
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQCODE_READ_EXTERNAL_STORAGE);
        }
    }

    public void sortBy(int id) {
        switch (id) { //menu sort id
            case R.id.menu_sort_by_name: {
                // sort by name
                Collections.sort(myAlbums, new Comparator<MyAlbum>() {
                    @Override
                    public int compare(MyAlbum t1, MyAlbum t2) {
                        return t1.getName().compareToIgnoreCase(t2.getName());
                    }
                });
                break;
            }
            case R.id.menu_sort_by_datetaken: {
                //sort by datetaken
                //only for images, not albums

                break;
            }
            case R.id.menu_sort_by_size: {
                //sort by size
                Collections.sort(myAlbums, new Comparator<MyAlbum>() {
                    @Override
                    public int compare(MyAlbum t1, MyAlbum t2) {
                        return (int) (t1.getRawAlbumSize() - t2.getRawAlbumSize());
                    }
                });

                break;
            }
        }
        adapterAlbum.notifyDataSetChanged();
        //move list view to top
        lvAlbums.smoothScrollToPosition(0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQCODE_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted
                    setupGallery();
                } else
                    System.exit(0);
                break;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mOnGalleryClickListener = (onGalleryClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onGalleryClickListener");
        }
    }

    @Override
    public void onDetach() {
        mOnGalleryClickListener = null;
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("RESUME", "albumList resume");

        co = new ContentObserver(null) {
            @Override
            public void onChange(boolean selfChange, Uri uri) {
                //todo: refresh here
                new actionRefresh().execute();
            }
        };
        cr = getContext().getContentResolver();
        cr.registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                , true
                , co);

    }

    @Override
    public void onPause() {
        super.onPause();
        cr.unregisterContentObserver(co);
    }

    public class actionRefresh extends AsyncTask<String, Void, Void> {
        ArrayList<MyAlbum> result;

        @Override
        protected void onPreExecute() {
            myAlbums.clear();
            result = new ArrayList<>();
            adapterAlbum = null;
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... strings) {
            Log.d("REFRESH", "doInBackground start");
            result = gallery.refreshAlbum(getContext(), getContext().getResources().getString(R.string.app_name));

            Log.d("REFRESH", "doInBackground finish");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("REFRESH", "onPostExecute start");

            myAlbums = new ArrayList<>(result);

            adapterAlbum = new AlbumListAdapter(getContext(), R.layout.album_item_for_list_view, myAlbums);
            lvAlbums.setAdapter(adapterAlbum);

            Log.d("REFRESH", "onPostExecute finish");
        }
    }


}
