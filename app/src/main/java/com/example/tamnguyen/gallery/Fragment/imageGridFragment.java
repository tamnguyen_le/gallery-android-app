package com.example.tamnguyen.gallery.Fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.tamnguyen.gallery.Activities.MainActivity;
import com.example.tamnguyen.gallery.Adapter.ImageGridAdapter;
import com.example.tamnguyen.gallery.Models.MyAlbum;
import com.example.tamnguyen.gallery.Models.MyImage;
import com.example.tamnguyen.gallery.Models.onAlbumImageClickListener;
import com.example.tamnguyen.gallery.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Tam Nguyen on 11-Nov-17.
 */

public class imageGridFragment extends Fragment {
    private static final String CURRENT_SORT_MODE = "CURRENT_SORT_MODE";

    @BindView(R.id.gridviewAlbums)
    GridView gvImages;
    ImageGridAdapter adapterImage;

    int currentSortMode;

    onAlbumImageClickListener mCallbacks;
    private MyAlbum album;
    ArrayList<MyImage> myImages;
    private ContentResolver cr;
    public static ContentObserver co;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.grid_layout_for_image, container, false);
        unbinder = ButterKnife.bind(this, v);

        setupUI();

        return v;
    }

    public void setupUI() {
        Bundle arguments = getArguments();
        album = (MyAlbum) arguments.getSerializable(MainActivity.CLICKED_ALBUM);

        if (album != null) {
            myImages = new ArrayList<>(album.getMyImages());
            adapterImage = new ImageGridAdapter(getContext(), R.layout.image_item_for_grid_view, myImages);

            gvImages.setAdapter(adapterImage);

            gvImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MyImage imageClicked = myImages.get(position);
                    //show image
                    mCallbacks.onImageClick(imageClicked);
                }
            });
        }
    }

    public void sortBy(int id) {
        switch (id) { //menu sort id
            case R.id.menu_sort_by_name: {
                // sort by name
                Collections.sort(myImages, new Comparator<MyImage>() {
                    @Override
                    public int compare(MyImage t1, MyImage t2) {
                        return t1.getDisplay_name().compareToIgnoreCase(t2.getDisplay_name());
                    }
                });
                currentSortMode = R.id.menu_sort_by_name;
                break;
            }
            case R.id.menu_sort_by_datetaken: {
                //sort by datetaken
                //only for images, not albums
                Collections.sort(myImages, new Comparator<MyImage>() {
                    @Override
                    public int compare(MyImage t1, MyImage t2) {
                        //convert to date
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date d1 = format.parse(t1.getRawDate_added());
                            Date d2 = format.parse(t2.getRawDate_added());
                            return d1.compareTo(d2);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return t1.getDate_added().compareToIgnoreCase(t2.getDate_added());
                    }
                });
                currentSortMode = R.id.menu_sort_by_datetaken;
                break;
            }
            case R.id.menu_sort_by_size: {
                //sort by size
                Collections.sort(myImages, new Comparator<MyImage>() {
                    @Override
                    public int compare(MyImage t1, MyImage t2) {
                        return (int) (Double.valueOf(t1.getRawSize()) -
                                Double.valueOf(t2.getRawSize()));
                    }
                });
                currentSortMode = R.id.menu_sort_by_size;
                break;
            }
        }
        adapterImage.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallbacks = (onAlbumImageClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onAlbumImageClickListener");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_SORT_MODE, currentSortMode);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null)
            currentSortMode = savedInstanceState.getInt(CURRENT_SORT_MODE);
    }

    @Override
    public void onResume() {
        super.onResume();

        co = new ContentObserver(null) {
            @Override
            public void onChange(boolean selfChange, Uri uri) {
                //todo: refresh here
                new actionRefresh().execute();
            }
        };
        cr = getContext().getContentResolver();
        cr.registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                , true
                , co);

//        if (ShowImageActivity.isBackWithSave == true) {
//            new actionRefresh().execute();
//            ShowImageActivity.isBackWithSave = false;
//        }

    }

    @Override
    public void onPause() {
        super.onPause();
        cr.unregisterContentObserver(co);
    }

    public class actionRefresh extends AsyncTask<String, Void, Void> {
        ArrayList<MyImage> result;

        @Override
        protected void onPreExecute() {
            myImages.clear();
            result = new ArrayList<>();
            adapterImage = null;
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... strings) {
            Log.d("REFRESH", "doInBackground start");
            result = album.refresh(getContext());
            // gallery.doRefresh(getContext());
            Log.d("REFRESH", "doInBackground finish");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("REFRESH", "onPostExecute start");

            myImages = new ArrayList<>(result);

            adapterImage = new ImageGridAdapter(getContext(), R.layout.image_item_for_grid_view, myImages);
            gvImages.setAdapter(adapterImage);

            Log.d("REFRESH", "onPostExecute finish");
        }
    }

}