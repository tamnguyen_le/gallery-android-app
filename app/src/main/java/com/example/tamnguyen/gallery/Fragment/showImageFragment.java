package com.example.tamnguyen.gallery.Fragment;


import android.content.ContentValues;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tamnguyen.gallery.Activities.MainActivity;
import com.example.tamnguyen.gallery.Models.MyImage;
import com.example.tamnguyen.gallery.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class showImageFragment extends Fragment {
    @BindView(R.id.ivShowImage)
    ImageView ivShowImage;

    @BindView(R.id.btnUpdateDes)
    Button btnUpdateDes;
    @BindView(R.id.txtEditDes)
    EditText edtDes;
    private Unbinder unbinder;

    public showImageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.show_image_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);

        //load image, set
        Bundle args = getArguments();
        final MyImage image = (MyImage) args.getSerializable(MainActivity.CLICKED_IMAGE);

        Glide.with(getContext()).load(image.getData()).apply(new RequestOptions().centerCrop()).into(ivShowImage);

        edtDes.setText(image.getDescription());

        btnUpdateDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDescription(image);
            }
        });
        return v;
    }

    public void updateDescription(MyImage image) {
        String newString = edtDes.getText().toString();

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DESCRIPTION, newString);
        String where = MediaStore.Images.ImageColumns._ID + " LIKE ?";

        getContext().getContentResolver().update(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values, where, new String[]{image.getId()});
    }
}
