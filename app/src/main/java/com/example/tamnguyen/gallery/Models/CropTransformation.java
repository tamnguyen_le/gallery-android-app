package com.example.tamnguyen.gallery.Models;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.support.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

/**
 * Created by Tam Nguyen on 12/2/2017.
 */

public class CropTransformation extends BitmapTransformation {

    private Rect cropZone;

    @Override
    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {

        int width = cropZone.right - cropZone.left;
        int height = cropZone.bottom - cropZone.top;

        return Bitmap.createBitmap(toTransform, cropZone.left, cropZone.top, width, height);
    }

    @Override
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(("Crop").getBytes());
    }
}
