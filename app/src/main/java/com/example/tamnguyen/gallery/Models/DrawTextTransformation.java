package com.example.tamnguyen.gallery.Models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;


/**
 * Created by BI on 05-12-17.
 */

public class DrawTextTransformation extends BitmapTransformation {
    String text;
    Integer color;
    int size;

    public DrawTextTransformation(Context context, String text, Integer color, int size) {
        super(context);
        this.text = text;
        this.color = color;
        this.size = size;
    }

    @Override
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(("drawtext").getBytes());
    }

    @Override
    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {

        // resource bitmaps are imutable,
        // so we need to convert it to mutable one

        Bitmap bitmap = toTransform.copy(toTransform.getConfig(), true);

        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(color);
        // text size in pixels
        paint.setTextSize(size);
        paint.setStyle(Paint.Style.FILL);
        // text shadow
        paint.setShadowLayer(10f, 10f, 10f, Color.BLACK);

        // draw text to the Canvas center


        Rect bounds = new Rect();
        paint.getTextBounds(this.text, 0, this.text.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width()) / 2;
        int y = (bitmap.getHeight() + bounds.height()) / 2;
        canvas.drawText(this.text, 0, bounds.height(), paint);

        return bitmap;
    }
}
