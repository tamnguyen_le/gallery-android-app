package com.example.tamnguyen.gallery.Models;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Tam Nguyen on 24-Nov-17.
 */

@GlideModule
public class GlideAppModule extends AppGlideModule{
}
