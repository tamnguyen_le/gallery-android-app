package com.example.tamnguyen.gallery.Models;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.app.Activity;
import android.widget.Toast;

import com.example.tamnguyen.gallery.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.content.ContentValues.TAG;

/**
 * Created by Tam Nguyen on 08-Dec-17.
 */

public class ImageSaver {

    public static void storeImage(Bitmap image, String fileName, Context context) {
        File pictureFile = getOutputMediaFile(fileName, context);
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            if (image.compress(Bitmap.CompressFormat.JPEG, 100, fos))
                Toast.makeText(context, "Image Saved", Toast.LENGTH_LONG);
            else
                Toast.makeText(context, "Failed", Toast.LENGTH_LONG);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }

        broadcastLastTakenImage(pictureFile.getPath(), context);
    }

    @Nullable
    private static File getOutputMediaFile(String imageName, Context context){

        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath()
                + File.separator
                + context.getResources().getString(R.string.app_name));

        // Create the storage directory if it does not exist
        if (! storageDir.exists()){
            if (! storageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String fullImageName= imageName + ".jpg";
        File mediaFile = new File(storageDir.getPath(), fullImageName);
        return mediaFile;
    }

    public static void broadcastLastTakenImage(String filePath, Context context) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }
}
