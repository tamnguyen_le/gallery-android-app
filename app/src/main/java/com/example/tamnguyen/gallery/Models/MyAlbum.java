package com.example.tamnguyen.gallery.Models;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by BI on 05-11-17.
 */

public class MyAlbum implements Serializable {
    private String thumbnailPath;
    private String name;
    private ArrayList<MyImage> myImages;

    public MyAlbum(String thumbnailPath, String name) {
        this.thumbnailPath = thumbnailPath;
        this.name = name;
        myImages = new ArrayList<MyImage>();
    }

    public MyAlbum(String thumbnailPath, String name, ArrayList<MyImage> myImages) {
        this.thumbnailPath = thumbnailPath;
        this.name = name;
        this.myImages = myImages;
    }

    public ArrayList<MyImage> refresh(Context context) {
        myImages.clear();
        String defaultOrderASC =
                MediaStore.Images.Media.DISPLAY_NAME
                        + " ASC";

        if (!this.getName().equals("All Images")) {
            ContentResolver contentResolver = context.getContentResolver();

            String where = MediaStore.Images.Media.BUCKET_DISPLAY_NAME + " LIKE ?";
            Cursor cr = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, where,
                    new String[]{this.name}, defaultOrderASC);

            cr.moveToPosition(-1);
            while (cr.moveToNext()) {
                MyImage temp = new MyImage(cr);
                myImages.add(temp);
            }
            cr.close();
        } else {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor cr = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, defaultOrderASC);

            cr.moveToPosition(-1);
            while (cr.moveToNext()) {
                MyImage temp = new MyImage(cr);
                myImages.add(temp);
            }
            cr.close();
        }

        return myImages;
    }

    public void addImage(MyImage image) {
        myImages.add(image);
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<MyImage> getMyImages() {
        return myImages;
    }

    public void setMyImages(ArrayList<MyImage> myImages) {
        this.myImages = myImages;
    }

    public String getNumOfPhotos() {

        int n_pics = myImages.size();

        if (1 == n_pics)
            return String.valueOf(n_pics) + " photo";

        return String.valueOf(n_pics) + " photos";
    }

    private String convertSize(String size) {
        int sizeByte = Integer.valueOf(size);
        int MBtoByte = 1024 * 1024;
        int KBtoByte = 1024;

        String param;
        double result;

        if (sizeByte <= MBtoByte) {
            param = "KB";
            result = (double) sizeByte / KBtoByte;
        } else {
            param = "MB";
            result = (double) sizeByte / MBtoByte;
        }

        result = Math.round(result * 100);
        result = result / 100;

        return String.valueOf(result) + " " + param;
    }

    public String getAlbumSize() {

        Integer totalSize = 0;

        for (MyImage _image : myImages) {
            Log.d("BREAK", _image.getData());
            totalSize += Integer.valueOf(_image.getRawSize());
        }

        return "Album size: " + convertSize(String.valueOf(totalSize));
    }

    public double getRawAlbumSize() {

        double totalSize = 0;

        for (MyImage _image : myImages)
            totalSize += Double.valueOf(_image.getRawSize());

        return totalSize;
    }

    //default when first loaded
    public void sortByName() {
        Collections.sort(myImages, new Comparator<MyImage>() {
            @Override
            public int compare(MyImage t1, MyImage t2) {
                return t1.getDisplay_name().compareTo(t2.getDisplay_name());
            }
        });
    }
}
