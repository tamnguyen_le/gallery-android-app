package com.example.tamnguyen.gallery.Models;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.tamnguyen.gallery.Activities.MainActivity;
import com.example.tamnguyen.gallery.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by BI on 12-11-17.
 */

public class MyCamera {
    private Activity activity;
    public String filePath;
    public String imageFileName;

    public MyCamera(MainActivity _actActivity) {
        activity = _actActivity;
    }

    public void startCameraIntent() {
        //TODO: check permisssion
        int percheckCAM = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int percheckWRITEEXTERNAL = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (percheckCAM == PackageManager.PERMISSION_GRANTED && percheckWRITEEXTERNAL == PackageManager.PERMISSION_GRANTED) {
            startCamera();
        } else ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MainActivity.REQCODE_CAMERA);
    }

    private void startCamera() {
        File imageFile = null;

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //TODO: put uri file here
        try {
            imageFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (imageFile != null) {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
            activity.startActivityForResult(cameraIntent, MainActivity.REQCODE_CAMERA_RESULT);
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_" + ".jpg";

        String storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath()
                + File.separator
                + activity.getResources().getString(R.string.app_name);

        //TODO: check if folder exists, if not, create new one
        File storageFolder = new File(storageDir);
        if (!storageFolder.exists())
            storageFolder.mkdir();

        File image = new File(storageDir, imageFileName);
        filePath = image.getAbsolutePath();

        return image;
    }

    public void broadcastLastTakenImage() {
//        //broadcast
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);

        //scanPhoto(filePath);
        //activity.getContentResolver().notifyChange(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null);
    }
}
