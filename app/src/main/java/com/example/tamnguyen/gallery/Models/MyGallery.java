package com.example.tamnguyen.gallery.Models;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.example.tamnguyen.gallery.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by BI on 05-11-17.
 */

public class MyGallery {
    private ArrayList<MyAlbum> myAlbums;
    private ArrayList<String> albumNames;

    public ArrayList<MyAlbum> getMyAlbums() {
        return myAlbums;
    }

    public void setMyAlbums(ArrayList<MyAlbum> myAlbums) {
        this.myAlbums = myAlbums;
    }

    public static String convertDateToFormat(String dateAddedOldFormat) {
        if (dateAddedOldFormat != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(dateAddedOldFormat));
            Date dateNewFormat = calendar.getTime();

            DateFormat df = new SimpleDateFormat("dd/MM/YYYY");
            String result = df.format(dateNewFormat);

            return result;
        }
        return "";
    }


    public MyGallery(Context mContext) {
        albumNames = new ArrayList<>();
        myAlbums = new ArrayList<>();

        //new album "All Images"
        MyAlbum allImagesAlbum = new MyAlbum("", "All Images");
        albumNames.add(allImagesAlbum.getName());
        myAlbums.add(allImagesAlbum);

        //default sort order: Album name ascending
        String defaultOrderASC =
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME
                        + " ASC";
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor cr = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, null,
                null, defaultOrderASC);

        cr.setNotificationUri(contentResolver, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        cr.moveToFirst();
        while (cr.moveToNext()) {
            //query 1 image data
            String albumThumbPath = cr.getString(cr.getColumnIndex(MediaStore.Images.Thumbnails.DATA));
            String albumName = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
            String imageId = cr.getString(cr.getColumnIndex(MediaStore.Images.Media._ID));
            String imagePath = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.DATA));
            String dateTaken = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
            String displayName = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
            String height = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.HEIGHT));
            String size = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.SIZE));
            String width = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.WIDTH));
            String description = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.DESCRIPTION));
            String isprivate = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.IS_PRIVATE));
            String ladtitude = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.LATITUDE));
            String longtitude = cr.getString(cr.getColumnIndex(MediaStore.Images.Media.LONGITUDE));

            //convert dateTaken,size
            String dateCvt = convertDateToFormat(dateTaken);

            if (!albumNames.contains(albumName)) {
                //not in album
                //get thumnail bitmap
                MyAlbum album = new MyAlbum(albumThumbPath, albumName);

                myAlbums.add(album);
                albumNames.add(albumName);
            }

            for (MyAlbum album : myAlbums) {
                if (album.getName().equals(albumName)) {
                    //add bitmap
                    MyImage image = new MyImage(albumName, imageId, imagePath, dateCvt
                            , displayName, height, size, width,
                            description, isprivate, ladtitude, longtitude);
                    album.addImage(image);
                    allImagesAlbum.addImage(image);
                }
            }
        }

        for (MyAlbum album : myAlbums) {
            //sort by name of images in album (first loaded)
            album.sortByName();
        }

        //set thumbnail for All Images
        if (allImagesAlbum.getThumbnailPath().equals("")) {
            String thumbPath = allImagesAlbum.getMyImages().get(0).getData();
            allImagesAlbum.setThumbnailPath(thumbPath);
        }

        cr.close();
    }

    public ArrayList<MyAlbum> refreshAlbum(Context mContext, String albumName) {
        int albumPos = -1;
        for (int i = 0; i < myAlbums.size(); i++) {
            if (myAlbums.get(i).getName().equals(albumName))
                albumPos = i;
        }

        //i = -1 , no albums exist
        if (albumPos == -1) {
            MyAlbum album = new MyAlbum("", "");
            album.setName(mContext.getResources().getString(R.string.app_name));
            album.refresh(mContext);
            if (album.getMyImages().size() != 0)
                album.setThumbnailPath(album.getMyImages().get(0).getData());
            myAlbums.add(album);
        }
        //i !=-1
        if (albumPos != -1) {
            myAlbums.get(albumPos).refresh(mContext);
        }

        return this.getMyAlbums();
    }

}
