package com.example.tamnguyen.gallery.Models;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.MediaStore;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by BI on 05-11-17.
 */

public class MyImage implements Serializable {
    private String albumName; //album that contains image
    private String id; //id of image
    private String data; //image file path
    private String date_added; //date add
    private String display_name; //name of image
    private String height;
    private String size;
    private String width;
    private String description; //description, tags
    private String is_private; //if image is private
    private String ladtitude, longtitude; //position taken

    public MyImage(String albumName, String id, String data, String date_added, String display_name, String height, String size, String width, String description, String is_private, String ladtitude, String longtitude) {
        this.albumName = albumName;
        this.id = id;
        this.data = data;
        this.date_added = date_added;
        this.display_name = display_name;
        this.height = height;
        this.size = size;
        this.width = width;
        this.description = description;
        this.is_private = is_private;
        this.ladtitude = ladtitude;
        this.longtitude = longtitude;
    }

    public MyImage(Cursor cs) {
        //query 1 image data
        this.albumName = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
        this.id = cs.getString(cs.getColumnIndex(MediaStore.Images.Media._ID));
        this.data = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.DATA));
        String dateTaken = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
        this.display_name = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
        this.height = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.HEIGHT));
        this.size = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.SIZE));
        this.width = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.WIDTH));
        this.description = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.DESCRIPTION));
        this.is_private = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.IS_PRIVATE));
        this.ladtitude = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.LATITUDE));
        this.longtitude = cs.getString(cs.getColumnIndex(MediaStore.Images.Media.LONGITUDE));

        //convert dateTaken,size
        this.date_added = convertDateToFormat(dateTaken);
    }

    private String convertDateToFormat(String dateAddedOldFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(dateAddedOldFormat));
        Date dateNewFormat = calendar.getTime();

        DateFormat df = new SimpleDateFormat("dd/MM/YYYY");
        String result = df.format(dateNewFormat);

        return result;
    }

    private String convertSize(String size) {
        int sizeByte = Integer.valueOf(size);
        int MBtoByte = 1024 * 1024;
        int KBtoByte = 1024;

        String param;
        double result;

        if (sizeByte <= MBtoByte) {
            param = "KB";
            result = sizeByte / KBtoByte;
        } else {
            param = "MB";
            result = sizeByte / MBtoByte;
        }

        return String.valueOf(result) + " " + param;
    }

    public String getId() {
        return id;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getData() {
        return data;
    }

    public String getDate_added() {
        return "Day added: " + date_added;
    }

    public String getRawDate_added() {
        return date_added;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public String getSize() {
        return "Size: " + convertSize(size);
    }

    public String getRawSize() {
        return size;
    }

    public String getDescription() {
        return description;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLadtitude(String ladtitude) {
        this.ladtitude = ladtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

}
