package com.example.tamnguyen.gallery.Models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

/**
 * Created by Tam Nguyen on 12/2/2017.
 */

public class RotateTransformation extends BitmapTransformation {

    private float rotationAngle = 0f;

    public RotateTransformation(Context context, float rotationAngle) {

        super(context);
        this.rotationAngle = rotationAngle;
    }

    @Override
    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {
        // Use ARGB_8888 since we're going to add alpha to the image.
        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle);
        Bitmap result = Bitmap.createBitmap(toTransform, 0, 0, toTransform.getWidth(), toTransform.getHeight(), matrix, true);
        return result;

    }

    @Override
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(("rotate" + rotationAngle).getBytes());
    }

}